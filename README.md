## Credentials
* Create your credential: https://www.binance.com/en/support/faq/360002502072
* If you want to test your bot, create your credential on testnet server: https://www.binance.com/en/support/faq/ab78f9a1b8824cf0a106b4229c76496d
* Create telegram bot to receive message  from your trading bot: https://dev.to/rizkyrajitha/get-notifications-with-telegram-bot-537l
=> Input credentials in credential_info.json

## Setup
* Install additional packages

```
$ pip3 install -r /path/to/requirements.txt
```
* Run local trading bot: 

```
python3 local-bot/__main__.py
```

