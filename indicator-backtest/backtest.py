from binance.enums import *
import pandas as pd
import pandas_ta as ta
import numpy as np
import talib
import os
cash_per_trade=100

class backtest:
    def __init__(self,symbol,timeframe):
        self.symbol = symbol
        self.timeframe = timeframe
        self.df = pd.read_csv(f"path\{self.symbol}\{timeframe}.csv")
        self.df.drop(['Close time', 'Quote asset volume'], axis=1, inplace=True)
        self.total_profit : list = []
        if self.df.empty:
            print ("Invalid input")
        else:
            self.indicator()
            self.buy_condition()
            self.sell_condition()
            self.SL_threshold()
            self.sell_signal()

    def indicator(self):
        self.df['sar1'] = talib.SAR(self.df['High'],self.df['Low'],acceleration=0.02,maximum=0.2)
        self.df['sar_percent'] = (self.df['Close'] / self.df['sar1']) -1
        self.df['ADX'] = (ta.adx(high=self.df['High'], close=self.df['Close'], low= self.df['Low'],adxlen=14, dilen=14, thold =20 ))['ADX_14']
        self.df['RSI'] = ta.rsi(close = self.df['Close']).round(decimals=2)
        # df1['EMA200'] = ta.ema(close=df1['Close'], length= 200)
        self.df['EMA of RSI'] = ta.ema(close = self.df['RSI'], length= 14)
        self.df['RSI/EMA']=self.df['RSI']/self.df['EMA of RSI']
        self.df.dropna(inplace=True)
        self.df.reset_index(inplace=True)
        self.df.drop('index',inplace=True, axis=1)

    def buy_condition(self):
        SAR_condition = self.df['sar1'] < self.df['Close'] 
        SAR_percent_condition = self.df['sar_percent'] < 0.05
        ADX_condition = ( self.df['ADX'] < 33 )
        RSI_condition = (46 < self.df['RSI']) & (54 > self.df['RSI']) & (self.df['EMA of RSI'] < self.df['RSI']) & (self.df['EMA of RSI'] > 43)
        # EMA_condition = ( df['EMA200'] < 236 )
        SAR_buy= SAR_condition.diff()
        self.df['buy_signal']=np.where((SAR_condition) & (SAR_buy==1), 1, 0)

        
    def sell_condition(self):
        SAR_condition = self.df['sar1'] > self.df['Close'] 
        SAR_sell = SAR_condition.diff()

        ADX_condition = ( self.df['ADX'] < 25 )
        ADX_sell = ADX_condition.diff()

        RSI_condition = (46 > self.df['RSI']) | (54 < self.df['RSI'])
        RSI_sell = RSI_condition.diff()

        self.sell_signal_demo=np.where(((SAR_condition) & (SAR_sell==1)) , 1, 0)

    def SL_threshold(self):
        avg_SL=[]
        avg_TP=[]
        for i in range(len(self.df)):
            if self.df.buy_signal.iloc[i]==1:
                k=1
                on_going=True
                while on_going and (i+k) < len(self.df):
                    min_value = None
                    max_value = None
                    if min_value is None or self.df.Low.iloc[i+k] < min_value: min_value = self.df.Low.iloc[i+k]
                    if max_value is None or self.df.High.iloc[i+k] > max_value: max_value = self.df.High.iloc[i+k]
                    if self.sell_signal_demo[i+k]==1:
                        if self.df['Close'].iloc[i+k] > self.df['Close'].iloc[i]:
                            on_going = False  
                            avg_SL.append((min_value/self.df.Close.iloc[i])-1)
                            avg_TP.append((max_value/self.df.Close.iloc[i])-1)
                            min_value = None 
                            max_value = None                  
                    k+=1
        self.SL = round(np.percentile(avg_SL,50), 4)
        self.TP = round(np.percentile(avg_TP,60), 4)
    
    def sell_signal(self):
        selldates=[]
        outcome=[]
        for i in range(len(self.df)):
            if self.df.buy_signal.iloc[i]==1:
                k=1
                SL_value = self.df.Close.iloc[i] * (1-self.SL)
                TP_value = self.df.Close.iloc[i] * (1+self.TP)
                on_going=True
                while on_going and (i+k) < len(self.df):
                    
                    if self.df.Low.iloc[i+k] <= SL_value:
                        selldates.append(self.df.Time.iloc[i+k])
                        outcome.append('SL1')
                        on_going=False
                    # elif df.High.iloc[i+k] >= TP_value:
                    #     selldates.append(df.Time.iloc[i+k])
                    #     outcome.append('TP1')
                    #     on_going=False

                    elif self.sell_signal_demo[i+k]==1:
                        if self.df['Close'].iloc[i+k] <= self.df['Close'].iloc[i]:
                            selldates.append(self.df.Time.iloc[i+k])
                            outcome.append('SL')
                            on_going=False  

                        elif self.df['Close'].iloc[i+k] >= self.df['Close'].iloc[i] :
                            selldates.append(self.df.Time.iloc[i+k])
                            outcome.append('TP')
                            on_going=False                
                    k+=1
        def categ(x):
            if x['Time'] in selldates:
                return 1
            else:
                return 0

        def categ1(x):
            if x['Time'] in selldates:
                return outcome[selldates.index(x['Time'])]
            else:
                return 0      

        self.df['sell_signal']=self.df.apply(lambda x: categ(x) , axis=1)
        self.df['outcome']=self.df.apply(lambda x: categ1(x), axis=1)
        self.df['Time']=pd.to_datetime(self.df['Time'],unit='ms') 

    def result(self):
        mask=self.df[(self.df['buy_signal']==1) | ((self.df['sell_signal']==1))]
        mask['Profit']=np.NaN
        for i in range(len(mask)):
            if  i-1 >= 0 :
                if mask['outcome'].iloc[i]=='TP':
                    mask['Profit'].iloc[i] = ((mask['Close'].iloc[i]/mask['Close'].iloc[i-1]) -1) * cash 
                
                elif mask['outcome'].iloc[i]=='SL':
                    mask['Profit'].iloc[i] = - (1-(mask['Close'].iloc[i]/mask['Close'].iloc[i-1])) * cash           

                elif mask['outcome'].iloc[i]=='SL1':
                    mask['Profit'].iloc[i] = - cash * self.SL        

                elif mask['outcome'].iloc[i]=='TP1':
                    mask['Profit'].iloc[i] =  cash * self.TP   

                elif mask['outcome'].iloc[i]==0:
                    mask['Profit'].iloc[i]=np.NaN
        mask.reset_index(inplace=True)
        mask.drop('index',inplace=True, axis=1)
        return  self.symbol , float(mask['Profit'].sum())