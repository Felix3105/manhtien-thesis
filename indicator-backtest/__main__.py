from backtest import backtest

list_coin = ['BTCUSDT','ETHUSDT','BNBUSDT','XRPUSDT','ADAUSDT','SOLUSDT','DOGEUSDT','DOTUSDT', 'MATICUSDT','AVAXUSDT','TRXUSDT','UNIUSDT','ETCUSDT','LTCUSDT','FTTUSDT','LINKUSDT']

if __name__ == '__main__':
    top_5 = {}
    for name in list_coin:
        name, profit = backtest(name,"1d").result()
        top_5 [name] = profit
    top_5 = sorted( top_5.items(), key=lambda pair: pair[1], reverse=True )[:5]
    print (top_5)
