import logging
logger=logging.getLogger()
import json
import websocket
import awswrangler as wr
import boto3

firehose_client = boto3.client('firehose', region_name = 'ap-southeast-1')
s3_client = boto3.client('s3')
symbols = wr.s3.read_json(path='s3://demo-stream-31/top_cap.json')[0].values.tolist()

class Websocket_connection:
    def __init__(self, symbol: list, testnet: bool, timeframe: str):
        if testnet:
            self.ws_url='wss://stream.binancefuture.com/ws'
        else: 
            self.ws_url='wss://fstream.binance.com/ws'    

        #Kinesis firehose
        self.records: list = []
        self.stream_name :str ='Your firehose delivery stream name'
        self.count = 1

        #Websocket
        self.id_ws: int = 1
        self.symbols = symbol
        self.timeframe :str = timeframe
        self.ws = None  
        self.start_ws()
        
    def on_error(self, ws, error):
        logger.error(error)

    def on_close(self, ws, close_status_code, close_msg):
        logger.warning("Closing connection")
        try:
            self.Close_ws('UNSUBSCRIBE')
        except Exception as e:
            print (e)
        
    def on_open(self, ws):
        try:
            self.Live_Klines('SUBSCRIBE')
        except Exception as e:
            print (e)
        logger.warning("Opened connection")
#       self.Live_ticker('ATOMUSDT','SUBSCRIBE')
    
    def start_ws(self):
        self.ws=websocket.WebSocketApp(self.ws_url, on_open=self.on_open, on_close=self.on_close,
                              on_error=self.on_error, on_message=self.on_message)
        while True:
            try: 
                self.ws.run_forever()
            except Exception as e:
                print (e)

    def Close_ws(self, method: str):
        data=dict()
        data['method']=method
        for symbol in self.symbols:
            data['params']=[symbol.lower()+'@kline_'+self.timeframe]
        data['id']=self.id_ws

    def on_message(self, ws, message: str):
        if self.count % 100 == 0:
            response = firehose_client.put_record_batch(
                DeliveryStreamName=self.stream_name,
                Records=self.records
            )
            self.records.clear()

        record = {
            "Data": json.dumps(message)
        }
        self.records.append(record)
        self.count = self.count + 1

        if len(self.records) > 0:
            response = firehose_client.put_record_batch(
                    DeliveryStreamName=self.stream_name,
                    Records= self.records
                )

    def Live_Klines (self, method: str):
        data=dict()
        data['method']=method
        data['params']=[]
        for symbol in self.symbols:
            data['params'].append(symbol.lower()+ '@kline_'+ self.timeframe)
        # m -> minutes; h -> hours; d -> days; w -> weeks; M -> months
        data['id']=self.id_ws

        try:
            self.ws.send(json.dumps(data))
        except Exception as e:
            print (e)  

        self.id_ws+=1

        
Websocket_connection(symbols,False,'1m')


