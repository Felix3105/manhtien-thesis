import boto3
import pandas as pd
import time
from binance.client import Client
import awswrangler as wr

s3_client = boto3.client('s3')
ec2_client = boto3.client('ec2', region_name='ap-southeast-1')

api_key='Specify your public key here'
api_secret='Specify your secret key here'
client = Client(api_key, api_secret)
timestamp = int(time.time()*1000) - 60000

# symbols = wr.s3.read_json(path='s3://demo-stream-31/top_cap.json')[0].values.tolist()

symbols = ['BTCUSDT','ATOMUSDT','ETHUSDT','SOLUSDT']
def getminutedata(symbol,interval,start, end):
    df = pd.DataFrame(client.get_historical_klines(symbol, interval, start, end ))    #futures_historical_klines, get_historical_klines
    df = df.iloc[:,:11]
    df.columns=['Time','Open','High','Low' ,'Close','Volume','Close_time','Quote_asset_volume', 'Number_of_trades', 'Taker_buy_base_asset_volume', 'Taker_buy_quote_asset_volume']
    df[['Open','High','Low' ,'Close','Volume','Quote_asset_volume', 'Number_of_trades','Taker_buy_base_asset_volume', 'Taker_buy_quote_asset_volume']]=df[['Open','High','Low' ,'Close','Volume','Quote_asset_volume', 'Number_of_trades','Taker_buy_base_asset_volume', 'Taker_buy_quote_asset_volume']].astype(float)
    df['par'] = symbol
    df.drop('Close_time', axis=1, inplace=True)
    return df

path = "s3://topcap-historicaldata-storage/raw"
bucket = 'topcap-historicaldata-storage'
partitions_types = {'par': 'string'}
columns_types = {
    'Time': 'bigint',
    'Open': 'double',
    'High': 'double',
    'Low': 'double',
    'Close': 'double',
    'Volume': 'double',
    'Quote asset volume': 'double',
    'Number of trades': 'double',
    'Taker buy base asset volume': 'double',
    'Taker buy quote asset volume': 'double'
}

database="demo-catalog-db"
table="demo-catalog-tb"
try:
    check_table = wr.catalog.table(database=database, table=table)
except:
    wr.catalog.create_csv_table(
        table=table,
        database=database,
        path=path,
        partitions_types=partitions_types,
        columns_types=columns_types,
    )
    print ('Create a new table')

for i in symbols:
    try:
        result = s3_client.list_objects_v2(Bucket=bucket, Prefix =f'par={i}' )
        if 'Contents' not in result:
            wr.s3.to_csv(
                df = getminutedata(i, '1m', 1663856400000, timestamp),
                path = path,
                index = False,
                filename_prefix = str(timestamp//60000 * 60000),
                dataset = True,
                database = database,
                table = table,
                partition_cols = ["par"],
                schema_evolution=True,
                dtype = columns_types
            )
        else:
            start_time = int(result['Contents'][-1]['Key'].replace('/','.').split('.')[1][0:13]) + 60000
            wr.s3.to_csv(
                df = getminutedata(i, '1m', start_time, timestamp),
                path = path,
                index = False,
                filename_prefix = str(timestamp//60000 * 60000),
                dataset = True,
                database = database,
                table = table,
                partition_cols = ["par"],
                schema_evolution=True,
                dtype = columns_types
            )
    except Exception as e:
        print (e)
        pass




