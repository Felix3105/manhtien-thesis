
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import pandas_ta as ta
import awswrangler as wr
import boto3


s3_client = boto3.client('s3')
timestream_client = boto3.client('timestream-write', region_name = "ap-northeast-1")

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)


# Import Dyanmic DataFrame class and import data from catalog to dynamic df
from awsglue.dynamicframe import DynamicFrame

dynamic_df = glueContext.create_dynamic_frame.from_catalog(
    database = "demo-database1",
    table_name = "test_table1"
) 
    
# Convert to Pandas df
from pyspark.sql import functions as F
spark_df = dynamic_df.toDF()

from pyspark.sql.types import TimestampType
spark_df = spark_df.select('*', (F.from_unixtime(F.col("time")/1000).alias("timestamp")).cast(TimestampType())).drop('time')

spark_df.printSchema()

#ETL and write results to Timestream DB
df = spark_df.toPandas()
df.sort_values('timestamp').groupby('par')
symbols = df['par'].unique().tolist()

df.info()

def indicator(df):
    #Trend Indicators (50EMA and 200EMA)
    df['EMA50'] = ta.ema(df['close'], length = 50)
    df['EMA200'] = ta.ema(df['close'], length = 200)

    #Momentum Indicators (MACD, RSI)
    df['RSI'] = ta.rsi(close = df['close']).round(decimals=2)

    #Volume Indicators (MFI, OBV)
    df['MFI'] = ta.mfi(
        high = df['high'], 
        low = df['low'],
        close = df['close'], 
        volume = df['volume']
    )
    df['OBV'] = ta.obv(
        close = df['close'], 
        volume = df['volume']
    )
# Consider when calculate ema50, ema200,.. ,we need to retrive old data from timestream table to perform calculation 

def write_records(df):    
    my_session = boto3.Session(region_name="ap-northeast-1")
    dimensions_col = list(df)[1:]
    dimensions_col.remove('close')
    wr.timestream.write(
        df=df,
        database="demo-timestream-db",
        table="demo-timestream-tb",
        time_col="timestamp",
        measure_col = "close",
        dimensions_cols = dimensions_col,
        boto3_session=my_session,
    )

for symbol in symbols:
    indicator(df=df[df['par']==symbol])
    write_records(df = df[df['par']==symbol])
    
job.commit()
