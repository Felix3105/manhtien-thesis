from sagemaker_sklearn_extension.externals import Header
from sagemaker_sklearn_extension.impute import RobustImputer
from sagemaker_sklearn_extension.preprocessing import NALabelEncoder
from sagemaker_sklearn_extension.preprocessing import RobustOrdinalEncoder
from sagemaker_sklearn_extension.preprocessing import RobustStandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline

# Given a list of column names and target column name, Header can return the index
# for given column name
HEADER = Header(
    column_names=[
        'median_house_value', 'longitude', 'latitude', 'housing_median_age',
        'total_rooms', 'total_bedrooms', 'population', 'households',
        'median_income', 'ocean_proximity'
    ],
    target_column_name='median_house_value'
)


def build_feature_transform():
    """ Returns the model definition representing feature processing."""

    # These features contain a relatively small number of unique items. If this list is
    # modified, the value of the `num_categorical_features` hyperparameter must be adjusted to
    # reflect the number of features in the modified list. To make this change please find this
    # pipeline in the "Candidate Pipelines" section of the
    # SageMakerAutopilotCandidateDefinitionNotebook and change the value of
    # `num_categorical_features` in the `candidate_specific_static_hyperameters` dictionary.

    categorical = HEADER.as_feature_indices(['ocean_proximity'])

    # These features can be parsed as numeric.

    numeric = HEADER.as_feature_indices(
        [
            'longitude', 'latitude', 'housing_median_age', 'total_rooms',
            'total_bedrooms', 'population', 'households', 'median_income'
        ]
    )

    categorical_processors = Pipeline(
        steps=[
            (
                'robustordinalencoder',
                RobustOrdinalEncoder(threshold='auto', max_categories=100)
            )
        ]
    )

    numeric_processors = Pipeline(
        steps=[
            ('robustimputer',
             RobustImputer()), ('robuststandardscaler', RobustStandardScaler())
        ]
    )

    column_transformer = ColumnTransformer(
        transformers=[
            ('categorical_processing', categorical_processors,
             categorical), ('numeric_processing', numeric_processors, numeric)
        ]
    )

    return Pipeline(steps=[('column_transformer', column_transformer)])


def build_label_transform():
    """Returns the model definition representing feature processing."""

    return NALabelEncoder()
