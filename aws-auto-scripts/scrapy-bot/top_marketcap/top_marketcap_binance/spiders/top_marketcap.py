import scrapy
import json
import awswrangler as wr
import pandas as pd
import scrapydo

class TopMarketcapSpider(scrapy.Spider):
    name = 'top_marketcap'
    allowed_domains = ['binance.com']
    start_urls = ['https://www.binance.com/bapi/composite/v1/public/marketing/symbol/list']

    def parse(self, response):
        top_crypto={}
        stable_coin = ['USDT','USDC', 'BUSD', 'TUSD']
        data = json.loads(response.body)
        crypto = data.get('data')
        
        for i in crypto:
            if i['rank'] is not None and i['rank'] <= 30 and i['name'] not in stable_coin :
                top_crypto[i['rank']] = f"{i['name']}USDT"
            if len(top_crypto) >= 20:
                break
        a = pd.DataFrame.from_dict(top_crypto, orient='index')
        wr.s3.to_json(a, path = "s3://demo-stream-31/top_cap.json") 

# scrapydo.run_spider(TopMarketcapSpider)