
from top_marketcap_binance.spiders.top_marketcap import TopMarketcapSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

def handler(event,context):
    settings=get_project_settings()
    process = CrawlerProcess(settings)
    process.crawl(TopMarketcapSpider)
    process.start() 


