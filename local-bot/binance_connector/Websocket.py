import logging
logger=logging.getLogger()
import websocket
import json
import time
# import os
# import json

from strategies.strategy_main import Strategy_implement

class Websocket_connection:
    def __init__(self, symbol, testnet, timeframe) -> None:
        if testnet:
            self.ws_url='wss://stream.binancefuture.com/ws'
        else: 
            self.ws_url='wss://fstream.binance.com/ws'    
    
        self.ws = None  
        self.id_ws: int = 1
        self.symbol = symbol
        self.timeframe :str = timeframe

        try:
            self.Strategy_implement = Strategy_implement(
                testnet = testnet, 
                symbol = symbol, 
                timeframe= timeframe
            )
        except Exception as e:
            logger.error(f"Failed to initialize Strategy_implement class, error: {e}")

        try:
            self.start_ws()
        except Exception as e:
            logger.error(f"Failed to start websocket streaming, error: {e}")
        
    def on_error(self, ws, error):
        logger.error(error)

    def on_close(self, ws, close_status_code, close_msg):
        logger.warning("Closing connection")

        try:
            self.Close_ws('UNSUBSCRIBE')
        except Exception as e:
            logger.error(f"Failed to unsubscribe to channel, error: {e}")
        
    def on_open(self, ws):
        logger.warning("Opened connection")
        self.Live_Klines('SUBSCRIBE')
#       self.Live_ticker('ATOMUSDT','SUBSCRIBE')
    
    def start_ws(self):
        self.ws=websocket.WebSocketApp(self.ws_url, on_open=self.on_open, on_close=self.on_close,
                              on_error=self.on_error, on_message=self.on_message)
        while True:
            try:
                self.ws.run_forever()
            except Exception as e:
                logger.error(f"Error in websocket , error: {e}")
            time.sleep(2)
        
    def Close_ws(self, method: str):
        data=dict()
        data['method']=method
        data['params']=[self.symbol.lower()+'@kline_'+self.timeframe]
        data['id']=self.id_ws
        
    def on_message(self, ws, message): 
        self.Strategy_implement.new_candle(message)
        
    def Live_Klines (self, method: str):
        data=dict()
        data['method']=method
        data['params']=[]
        data['params'].append(self.symbol.lower()+ '@kline_'+ self.timeframe)
        # m -> minutes; h -> hours; d -> days; w -> weeks; M -> months
        data['id']=self.id_ws
        try:
            self.ws.send(json.dumps(data))
        except Exception as e:
                logger.error(f"Failed to send json.dumps, error: {e}")
        self.id_ws+=1

