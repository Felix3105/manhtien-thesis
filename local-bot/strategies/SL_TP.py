import pandas as pd
import numpy as np
import talib
from binance_connector.Binance import Binance_connector

def Find_SL_TP(df):
    df = df.iloc[:,:6]
    df.columns = ['Time','Open','High','Low' ,'Close','Volume']
    df[['Open','High','Low' ,'Close','Volume']] = df[['Open','High','Low' ,'Close','Volume']].astype(float)
    df['sar1'] = talib.SAR(df['High'],df['Low'],acceleration=0.02,maximum=0.2)
    df.dropna(inplace=True)
    Long = df['sar1'] < df['Close']
    Short = df['sar1'] > df['Close']
    df['Long_start_signal'] = np.where((Long) & (Long.diff()==1), 1, 0)
    df['Long_end_signal'] = np.where((Short) & (Short.diff()==1), 1, 0)
    df.dropna(inplace=True)
    avg_high=[]
    avg_low=[]
    for i in range(len(df)):
        if df['Long_start_signal'].iloc[i]==1:
            position=True
            k=1
            while (position==True) & (((i+k) < len(df))):
                if df['Long_end_signal'].iloc[i+k]==1:
                    position=False
                    avg_high.append(((df.loc[(i+1):(i+k),'High'].max())/df['Close'].iloc[i]) - 1)
                    avg_low.append(1-((df.loc[(i+1):(i+k),'Low'].min())/df['Close'].iloc[i]))
                k+=1
    TP=round(np.percentile(avg_high,80), 4)
    SL=round(np.percentile(avg_low,60), 4)    
    return TP , SL

connector = Binance_connector()
df = connector.feed_historical_candles()
TP, SL = Find_SL_TP(df)
print (TP,SL)