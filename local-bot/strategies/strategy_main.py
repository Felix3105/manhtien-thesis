
from urllib import response
import pandas as pd
import numpy as np
import talib
import json
import logging 
logger = logging.getLogger()

from binance_connector.Binance import Binance_connector
from telegram_bot.Telegram_bot import telegram_alert

class Strategy_implement:
    def __init__(self, testnet, symbol, timeframe):
        if timeframe[-1] == 'm':
            self.CandleLength_unix = 60000 * int(timeframe[:-1])
        else:
            self.CandleLength_unix = 60000 * 60 * 24 * int(timeframe[:-1])

        f = open("credential_info.json")
        credential_info = json.load(f)
        if testnet:
            credential = credential_info['testnet']
        else:
            credential = credential_info['real']
        
        try:
            self.Binance_connector = Binance_connector(
                public_key = credential['public_key'], 
                secret_key= credential['secret_key'], 
                testnet = testnet, 
                symbol = symbol,
                timeframe = timeframe
            )
        except Exception as e:
            logger.error(e)

        self.telegram_alert = telegram_alert(
            token = credential_info['telegram']['token'], 
            bot_id= credential_info['telegram']['bot_id']
        )

        self.current_timestamp = self.Binance_connector.get_server_time()
        self.open = list()
        self.close = list()
        self.high = list()
        self.low = list()
        self.current = float()
        self.sar1 = pd.Series(dtype='float64')
        self.entry_timestamp =int()
        self.order_log = list()

        self.take_profit: float = 0.04
        self.stop_loss: float = 0.0180
        
        self.order_quantity: float = 0.01
        self.ongoing_position: bool = False
        self.filled_status: bool = False
        self.new_order = list()
        self.profit = list()

    def new_candle(self, message):
        response=json.loads(message)['k']
        candle=[response['t'],response['o'],response['h'],response['l'],response['c'],response['x']]
        if len(candle)==6:
            if bool(candle[5]):
                # self.all_candle.append(candles)
                self.open.append(float(candle[1]))
                self.high.append(float(candle[2]))            
                self.low.append(float(candle[3]))
                self.close.append(float(candle[4]))
                self.current=(float(candle[4]))

                self.sar1= talib.SAR(
                    pd.Series(self.high),
                    pd.Series(self.low),
                    acceleration=0.02,
                    maximum=0.2
                )

                if self.ongoing_position==False:
                    self.check_trade()
                    # self.check_signal()
            else:
                self.current=(float(candle[4]))
        
            if self.ongoing_position==True:        
                self.order_trace() 

    def check_trade(self):
        check_signal_results= self.check_signal()     
        if check_signal_results == 1:
            self.open_position(check_signal_results)
        else:
            print ('Ongoing position: False')
        
    def check_signal(self):

        try:   
            if (self.sar1.iloc[-1] < self.close[-1]) & (self.sar1.iloc[-2] > self.close[-2]):
                    self.telegram_alert.send_text('First long signal')
                    return 1     #Long

            elif (self.sar1.iloc[-1] > self.close[-1]) & (self.sar1.iloc[-2] < self.close[-2]):
                    self.telegram_alert.send_text('First short signal')
                    return -1     #Short

            else: 
                return 0
        except Exception as e:
            logger.warning(f"Insufficient data for SAR, error: {e}.  Please wait a few periods")
            

    def open_position(self,check_signal_result):
        self.entry_timestamp = self.current_timestamp
        order_side = 'BUY' if check_signal_result == 1 else 'SELL'
        self.ongoing_position == True

        if order_side =='BUY':
            price = round(self.close[-1], 2)
            SL_price = round(self.close[-1] * (1-self.stop_loss), 2 )
            TP_price = round(self.close[-1] * (1+ self.take_profit), 2)

            try:
                order_info = self.Binance_connector.place_order(
                    order_side = order_side, 
                    quantity = self.order_quantity, 
                    type_order = 'LIMIT',  
                    price = price, 
                    timeInForce = 'GTC'
                )

                SL_order = self.Binance_connector.place_order(
                    order_side = 'SELL',
                    quantity = self.order_quantity,
                    type_order = 'STOP',  
                    price = SL_price, 
                    stopPrice=SL_price
                )

                TP_order = self.Binance_connector.place_order(
                    order_side = 'SELL',
                    quantity = self.order_quantity,
                    type_order = 'TAKE_PROFIT',  
                    price = TP_price, 
                    stopPrice= TP_price 
                )
            except Exception as e:
                logger.error("Failed to set up long orders, error: {e}")

        elif order_side=='SELL':
            price =round(self.close[-1], 2)
            SL_price= round(self.close[-1] * (1 + self.stop_loss) , 2 )
            TP_price= round(self.close[-1] * (1- self.take_profit), 2)

            try:
                order_info= self.Binance_connector.place_order(
                    order_side = order_side, 
                    quantity = self.order_quantity, 
                    type_order = 'LIMIT', 
                    price = price, 
                    timeInForce = 'GTC'
                )

                SL_order = self.Binance_connector.place_order(
                    order_side = 'BUY', 
                    quantity = self.order_quantity, 
                    type_order = 'STOP',
                    price = SL_price, 
                    stopPrice = SL_price 
                )

                TP_order= self.Binance_connector.place_order(
                    order_side = 'BUY', 
                    quantity = self.order_quantity, 
                    type_order = 'TAKE_PROFIT', 
                    price = TP_price, 
                    stopPrice = TP_price 
                )
            except Exception as e:
                logger.error("Failed to set up short orders, error: {e}")      
             #TYPE ORDER: LIMIT, MARKET,STOP, STOP_MARKET, TAKE_PROFIT, TAKE_PROFIT_MARKET, TRAILING_STOP_MARKET

        if order_info is not None:
    # "time" , "entry_price" , "side", "status" , "entry_id"
            if order_info['status'] == "NEW":
                self.new_order = [order_info['orderId'],order_side]
                # self.order_log.append(self.new_order)

                self.telegram_alert.send_text (
                    f"New order: {order_side} At: {str(pd.to_datetime(order_info['updateTime'],unit='ms'))} Price: {order_info['price']}"
                )

        if SL_order is not None:
            if SL_order['status']=='NEW':
                self.telegram_alert.send_text (f"Stop lost is set at: {str(pd.to_datetime(order_info['updateTime'],unit='ms'))}")

        if TP_order is not None:
            if SL_order['status']=='NEW':
                self.telegram_alert.send_text (f"Take profit is set at: {str(pd.to_datetime(order_info['updateTime'],unit='ms'))}")

    def order_trace(self):
        current_price= self.current

        try:
            position_info= self.Binance_connector.Request_position_information()
        except Exception as e:
            logger.error("Failed to request current position info, error: {e}")

        if not self.filled_status:
            if (self.entry_timestamp + self.CandleLength_unix >=  self.current_timestamp) and float(position_info[0]['positionAmt']) == 0:
                try:
                    cancel_order=self.Binance_connector.cancel_all_order()
                except Exception as e:
                    logger.error(f"Failed to cancel all order, error: {e}")

                if cancel_order['msg']=='The operation of cancel all open order is done.':
                    self.telegram_alert(f"{cancel_order['msg']} at {str(pd.to_datetime(self.current_timestamp,unit='ms'))}.")
                    self.ongoing_position = False
            # Order_log: "orderID" , "side".

            elif float(position_info[0]['positionAmt']) == self.order_quantity:
                self.filled_status = True
                self.telegram_alert.send_text (f"The order has been filled at: {str(pd.to_datetime(self.current_timestamp,unit='ms'))}")
        
        if self.filled_status:
            if float(position_info[0]['positionAmt']) == 0:
                response = self.Binance_connector.get_income_history(income_type= "REALIZED_PNL", startTime= self.entry_timestamp)
                realized_pnl = 0
                for i in response:
                    realized_pnl += float(i['income'])
                self.telegram_alert.send_text (f"The trade made ${realized_pnl} in profit at {str(pd.to_datetime(self.current_timestamp,unit='ms'))}")    
                self.ongoing_position = False
                self.filled_status = False

