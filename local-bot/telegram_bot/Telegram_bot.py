import requests

class telegram_alert:
    def __init__(self, token, bot_id):
        self.base_url ='https://api.telegram.org/bot'
        self.token=token
        self.bot_id=bot_id

    def send_text(self,message):
        data=dict()
        data['chat_id']=self.bot_id
        data['text']=message
        send_text = requests.get( self.base_url + self.token +'/sendMessage' , params=data)
        return send_text

    def send_audio(self):
        pass

    def send_document(self):
        pass

    def send_video(self):
        pass

    def send_photo(self):
        pass



# Ref:
# https://core.telegram.org/bots#keyboards
# https://core.telegram.org/bots/api#making-requests
# https://core.telegram.org/bots/api#available-methods



